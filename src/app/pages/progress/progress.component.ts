import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.css']
})
export class ProgressComponent implements OnInit {

  progress1: number = 25;
  progress2: number = 45;
  
  leftButton: string = 'btn btn-info';
  rightButton: string = 'btn btn-primary';

  constructor() { }

  ngOnInit(): void {
  }

  get getProgress1() {
    return `${this.progress1}%`;
  }

  get getProgress2() {
    return `${this.progress2}%`;
  }

  changeValueProgress1(newProgress1Value: number) {
    this.progress1 = newProgress1Value;
  }

  changeValueProgress2(newProgress2Value: number){
    this.progress2 = newProgress2Value;
  }
}
