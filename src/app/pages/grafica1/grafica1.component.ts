import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grafica1',
  templateUrl: './grafica1.component.html',
  styles: [
  ]
})
export class Grafica1Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  // Doughnut 1
  public title1: string = "Sales"
  public labels1: string[] = [ 'Download Sales 1', 'In-Store Sales 1', 'Mail-Order Sales 1' ];
  public data1: number[][] = [
      [ 350, 350, 100 ],
  ];

  public title2: string = "Purchases"
  public labels2: string[] = [ 'Download Sales 2', 'In-Store Sales 2', 'Mail-Order Sales 2' ];
  public data2: number[][] = [
      [ 350, 350, 100 ],
  ];

  public title3: string = "Refunds"
  public labels3: string[] = [ 'Download Sales 3', 'In-Store Sales 3', 'Mail-Order Sales 3' ];
  public data3: number[][] = [
      [ 350, 350, 100 ],
  ];

  public title4: string = "Stock"
  public labels4: string[] = [ 'Download Sales 4', 'In-Store Sales 4', 'Mail-Order Sales 4' ];
  public data4: number[][] = [
      [ 350, 350, 100 ],
  ];
/*
  // events
  public chartClicked({ event, active }: { event: ChartEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: ChartEvent, active: {}[] }): void {
    console.log(event, active);
  }*/

}
