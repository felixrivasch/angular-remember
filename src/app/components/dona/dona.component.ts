import { Component, Input, OnInit } from '@angular/core';
import { ChartType } from 'chart.js';
import { Color, MultiDataSet } from 'ng2-charts';

@Component({
  selector: 'app-dona',
  templateUrl: './dona.component.html',
  styleUrls: ['./dona.component.css']
})
export class DonaComponent implements OnInit {

  @Input('titleIn') title: string = 'Without title';
  @Input('labelsIn') labels: string[] = [ 'Download Sales', 'In-Store Sales', 'Mail-Order Sales' ];
  @Input('dataIn') data: number[][] = [
    [ 350, 450, 100 ],
  ];
  constructor() { }

  ngOnInit(): void {
  }

  public doughnutChartType: ChartType = 'doughnut';

  public colors: Color[] = [
    {backgroundColor: ['brown', 'blue', 'gray']}
  ]
}
