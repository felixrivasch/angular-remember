import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-augmentative',
  templateUrl: './augmentative.component.html',
  styleUrls: ['./augmentative.component.css']
})
export class AugmentativeComponent implements OnInit, AfterViewInit {

  @ViewChild('inputProgressValue') inputProgressValue: ElementRef;
  @Input('progressValue') progress: number = 50;
  @Input('btnClass') btnClass: string = 'btn-primary';

  @Output('emitProgressValue') outputValue: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    this.btnClass = `btn ${this.btnClass}`;
  }

  ngAfterViewInit() {
    console.log(this.inputProgressValue.nativeElement);
  }

  get percent() {
    return `${this.progress}%`;
  }

  addValue(value: number) {
    if (this.progress >= 100 && value >= 0) {
      this.outputValue.emit(100);
      return this.progress == 100;
    }
    if (this.progress <= 0 && value <= 0) {
      this.outputValue.emit(0)
      return this.progress == 0;
    }
    this.progress = this.progress + value;
    this.outputValue.emit(this.progress);
    return this.progress;
  }

  valueChange(newValue: number) {
    console.log('newValue', newValue);
    
    if (newValue >= 100) {
      this.inputProgressValue.nativeElement.value = 100;
      this.progress = 100;
    } else if(newValue <= 0) {
      this.inputProgressValue.nativeElement.value = 0;
      this.progress = 0;
    } else {
      this.inputProgressValue.nativeElement.value = newValue;
      this.progress = newValue;
    }
    console.log('this.progress', this.progress);
    
    this.outputValue.emit(this.progress);
  }
}
