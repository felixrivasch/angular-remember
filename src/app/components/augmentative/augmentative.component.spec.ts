import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AugmentativeComponent } from './augmentative.component';

describe('AugmentativeComponent', () => {
  let component: AugmentativeComponent;
  let fixture: ComponentFixture<AugmentativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AugmentativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AugmentativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
