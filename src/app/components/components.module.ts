import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AugmentativeComponent } from './augmentative/augmentative.component';
import { FormsModule } from '@angular/forms';
import { DonaComponent } from './dona/dona.component';
import { ChartsModule } from 'ng2-charts';



@NgModule({
  declarations: [
    AugmentativeComponent,
    DonaComponent
  ],
  exports: [
    AugmentativeComponent,
    DonaComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ChartsModule
  ]
})
export class ComponentsModule { }
